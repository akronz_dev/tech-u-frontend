import { ElementService } from './ElementService';
import { Elementbase } from '../../utils/ElementBase.js';

class ElementDm extends Elementbase{

  static get properties() {
    return {
      autoloadProducts: { type: Boolean },
      autoloadUsers: { type: Boolean }
    };
  }

  constructor() {
    super();
    this.init();
  }

  get elementService() {
    return new ElementService();
  }

  async init() {
    await this.updateComplete;
    if(this.autoloadProducts) {
      this.loadProducts();
    }
    if(this.autoloadUsers) {
      this.loadUsers();
    }
  }

  async loadProducts() {
    this.dispatch('before-load-products', true);
    let data = await this.elementService.listProducts();
    this.dispatch('load-products-complete', data);
  }

  async loadUsers(userName) {
    this.dispatch('before-load-users', true);
    let data;
    if (userName && userName.length > 0) {
      let { items } = await this.elementService.searchUsers(userName);
      data = items;
    } else {
      data = await this.elementService.listUsers();
    }
    this.dispatch('load-users-complete', data);
  }

}
customElements.define('element-dm', ElementDm);