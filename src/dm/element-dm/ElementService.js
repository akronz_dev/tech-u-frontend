export class ElementService {

   async listProducts() {
     return await fetch('https://gorest.co.in/public-api/products').then(response => response.json());
   }

   async listUsers() {
    return await fetch('https://api.github.com/users').then(response => response.json());
   }

   async searchUsers(userName) {
    return await fetch(`https://api.github.com/search/users?q=${userName}`).then(response => response.json());
   }

}