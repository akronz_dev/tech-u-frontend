import { html, css } from 'lit-element';
import { Elementbase } from '../../utils/ElementBase.js';

class HeaderMenu extends Elementbase {
  static get styles() {
    return css`
     a {
        cursor: pointer;
      }
    `
  }

  static get properties() {
    return {
            titleHeader: { type: String },
            options: { type: Array }
          };
  }

  toggleNavBar() {
    this.shadowRoot.querySelector('#navbarSupportedContent').classList.toggle('show');
  }

  constructor() {
    super();
    this.options = [];
    this.titleHeader = 'App Lit Element';
  }
  render() {
    return html`
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css"
      integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">

      <nav class="navbar navbar-expand-lg navbar-light bg-light" style = "border-bottom:1px solid #e1e1e1;" >
        <a class="navbar-brand" href="#">${this.titleHeader}</a>
        <button @click="${this.toggleNavBar}" class="navbar-toggler" type="button" data-toggle="collapse"
          data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false"
          aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
          <ul class="navbar-nav mr-auto">

            ${this.options.map(
              (item) =>
                html`
                 <li class="nav-item active">
                  <a class="nav-link" @click="${() => this.dispatch('option-menu-selected', item)}">${item.title}</a>
                </li>
                `
            )}
          </ul>
          <form class="form-inline my-2 my-lg-0">
            <input class="form-control col" placeholder="Buscar" @input = "${(e) => this.dispatch('on-search-user', e.target.value)}" aria-label="Search">
          </form>
        </div>
      </nav>

    `
  }
}
customElements.define('header-menu', HeaderMenu);