import { html, css } from 'lit-element';
import { Elementbase } from '../../utils/ElementBase.js';

class FormPersona extends Elementbase {
  static get styles() {
    return css`

    .invalid {
      border-color: #dc3545;
      padding-right: calc(1.5em + .75rem);
      background-image: url("data:image/svg+xml,%3csvg xmlns='http://www.w3.org/2000/svg' width='12' height='12' fill='none' stroke='%23dc3545' viewBox='0 0 12 12'%3e%3ccircle cx='6' cy='6' r='4.5'/%3e%3cpath stroke-linejoin='round' d='M5.8 3.6h.4L6 6.5z'/%3e%3ccircle cx='6' cy='8.2' r='.6' fill='%23dc3545' stroke='none'/%3e%3c/svg%3e");
      background-repeat: no-repeat;
      background-position: right calc(.375em + .1875rem) center;
      background-size: calc(.75em + .375rem) calc(.75em + .375rem);
    }

    `
  }

  static get properties() {
    return {
      persona: { type: Object }
    };
  }

  constructor() {
    super();
    this.persona = {};
  }

  validate() {
    let requires = this.shadowRoot.querySelectorAll('.required');
    let errors = 0;
    requires.forEach(element => {
      if (element.value.length === 0) {
        element.classList.add('invalid');
        errors++;
      } else {
        element.classList.remove('invalid');
      }
    });
    return (errors === 0);
  }

  changeInput(evt, field) {
    let tmp = {...this.persona};
    tmp[field] = evt.target.value;
    this.persona = tmp;
    this.requestUpdate();
  }

  getPersona() {
    return this.persona;
  }

  reset() {
    let inputs = this.shadowRoot.querySelectorAll('.form-control');
    inputs.forEach(element => {
      element.classList.remove('invalid');
    });
    this.persona = {};
    this.persona.name = '';
  }

  async setPersona(person) {
    this.persona = {};
    let tmp = {...person};
    this.persona = tmp;
    await this.requestUpdate();
  }

  render() {
    return html`
     <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css"
      integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">

      <div class="form-group">
        <label for="exampleFormControlInput1">Nombre</label>
        <input class="form-control required" .value = "${this.persona.name ? this.persona.name : ''}" @input = "${(evt) => this.changeInput(evt, 'name')}" >
      </div>

      <div class="form-group">
        <label for="exampleFormControlInput1">Imagen</label>
        <input class="form-control" .value = "${this.persona.photo ? this.persona.photo : ''}" @input = "${(evt) => this.changeInput(evt, 'photo')}" >
      </div>

      <div class="form-group">
        <label for="exampleFormControlInput1">Pagina personal</label>
        <input class="form-control" .value = "${this.persona.page ? this.persona.page : ''}" @input = "${(evt) => this.changeInput(evt, 'page')}">
      </div>

    `
  }
}
customElements.define('form-persona', FormPersona);