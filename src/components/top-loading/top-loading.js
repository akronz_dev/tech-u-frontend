import { html, css } from 'lit-element';
import { Elementbase } from '../../utils/ElementBase';

class TopLoading extends Elementbase {
  static get styles() {
    return css`

    :host([hidden]), [hidden] {
      display: none !important;
    }

    .slider{
        top:0;
      position: fixed;
      z-index: 10;
      width: 100vw;
      height:5px;
      overflow-x: hidden;
    }

    .line{
      position:absolute;
      opacity: 0.4;
      background:#388D4F;
      width:150%;
      height:5px;
    }

    .subline{
      position:absolute;
      background:#388D4F;
      height:5px;
    }

    .inc{
      animation: increase 2s infinite;
    }
    .dec{
      animation: decrease 2s 0.5s infinite;
    }

    @keyframes increase {
      from { left: -5%; width: 5%; }
      to { left: 130%; width: 100%;}
    }
    @keyframes decrease {
      from { left: -80%; width: 80%; }
      to { left: 110%; width: 10%;}
    }

    `
  }

  static get properties() {
    return { show: { type: Boolean } };
  }

  render() {
    return html`
    <div class="slider" ?hidden = "${!this.show}">
      <div class="line"></div>
      <div class="subline inc"></div>
      <div class="subline dec"></div>
    </div>
    `
  }
}
customElements.define('top-loading', TopLoading);