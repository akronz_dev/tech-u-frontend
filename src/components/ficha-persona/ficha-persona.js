import { html, css } from 'lit-element';
import { Elementbase } from '../../utils/ElementBase.js';

class FichaPersona extends Elementbase {
  static get styles() {
    return css`

    .card {
      margin-bottom: 15px;
      min-height: 330px;
    }

    .card-img-top {
      width:80%;
      margin: 15px auto 0 auto;
      text-align:center;
      background-color: #fff;
    }

    .card-text {
      color: #999;
      line-height: 1.3em;
    }

    a {
      color: #999;
      transition: all 0.5s;
      text-decoration: none !important;
    }

    a:hover {
      color: #000;
    }

    .container-buttons {
      display: flex;
      align-items: center;
      justify-content: space-between;
    }

    `
  }

  static get properties() {
    return {
      persona: { type: Object }
    };
  }

  constructor() {
    super();
    this.persona = {};

  }

  removePersona() {
    this.dispatch('on-remove-person', this.persona);
  }

  editPersona() {
    this.dispatch('on-edit-person', this.persona);
  }

  get titleCard() {
    let link = html`${this.persona.name ? this.persona.name : 'Anonymous'}`;
    if(this.persona.page) {
      link = html`${this.persona.name ? html`<a href = "${this.persona.page ? this.persona.page : 'javascript:;'}" target = "_blank" >${this.persona.name}</a>` : 'Anonymous'}`;
    }
    return html`<h5 class="card-title text-center">${link}</h5>`
  }

  render() {
    return html`

<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css"
  integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">

<div class="card">
  <img
    src="${this.persona.photo ? this.persona.photo : this.persona.gender === 'F' ? './assets/famele.png' : './assets/avatar.png'}"
    class="card-img-top" alt="./assets/avatar.png">
  <div class="card-body">
    ${this.titleCard}
    <div class = "container-buttons">
      <a @click = "${this.removePersona}" class="btn btn-secondary">Eliminar</a>
      <a @click = "${this.editPersona}" class="btn btn-info">Modificar</a>
    </div>

  </div>
</div>

      `
  }
}
customElements.define('ficha-persona', FichaPersona);