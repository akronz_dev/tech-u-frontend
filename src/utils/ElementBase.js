import { LitElement } from 'lit-element';

export class Elementbase extends LitElement {

  constructor() {
    super();
  }

  dispatch(eventName, detail) {
    const customEvent = new CustomEvent(eventName, {
      detail: detail || {},
      bubbles: true,
      composed: true
    });
    this.dispatchEvent(customEvent);
  }

  makeid(length) {
    length = length || 16;
    var result = '';
    var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var charactersLength = characters.length;
    for (var i = 0; i < length; i++) {
      result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
  }

}