import { LitElement, html, css } from 'lit-element';
import './components/ficha-persona/ficha-persona';
import './dm/element-dm/element-dm';
import './components/header-menu/header-menu';
import './components/form-persona/form-persona';
import './components/top-loading/top-loading';
import { Elementbase } from './utils/ElementBase';

export class PersonaApp extends Elementbase {
  static get properties() {
    return {
      personas: { type: Array },
      options: { type: Array },
      delayPressSearch: { type: Number },
      personaSelected: { type: Object },
      modeUpdate: { type: Boolean }
    };
  }

  static get styles() {
    return css`

      :host{
        margin:0;
        padding:0;
      }

      :host([hidden]), [hidden] {
        display: none !important;
      }

      main {
        padding: 20px 0;
      }

      .modal-open-wc {
        position: fixed;
        top: 0;
        left: 0;
        z-index: 1050;
        width: 100%;
        height: 100%;
        overflow: hidden;
        outline: 0;
        background-color: rgb(0,0,0,0.7);
      }

      a {
        cursor: pointer;
      }

      .hide {
        display: none !important;
      }

    `;
  }

  constructor() {
    super();
    this.personas = [];
    this.personaSelected = {};
    this.delayPressSearch = 0;
    this.options = [
      {
        name: 'create',
        title: 'Agregar'
      }
    ];
  }

  removePerson(person) {
    let tmpArray = this.personas.filter(item => item.id !== person.id);
    this.personas = tmpArray;
    this.requestUpdate();
  }

  async editPerson(person) {
    console.log(person);
    this.personaSelected = person;
    await this.shadowRoot.querySelector('form-persona').setPersona(person);
    this.modeUpdate = true;
    this.openModal();
  }

  loadUsers(data) {
    this.personas = data.map(item => {
      return {
        id: item.id,
        photo: item.avatar_url,
        name: item.login,
        page: item.html_url
      };
    });

    if(data.length === 0) {
      this.shadowRoot.querySelector('.alert').classList.remove('hide');
      this.shadowRoot.querySelector('.alert').innerHTML = 'No se encontraron resultados para mostrar';
    } else {
      this.shadowRoot.querySelector('.alert').innerHTML = 'Consultando la informacion de las perosnas';
      this.shadowRoot.querySelector('.alert').classList.add('hide');
    }

    this.shadowRoot.querySelector('top-loading').show = false;
  }

  beforeLoadUsers() {
    this.shadowRoot.querySelector('.alert').classList.remove('hide');
    this.shadowRoot.querySelector('top-loading').show = true;
  }

  openModal() {
    this.shadowRoot.querySelector('#exampleModal').style.display = 'block';
    this.shadowRoot.querySelector('#exampleModal').classList.add('show');
    this.shadowRoot.querySelector('#exampleModal').classList.add('modal-open-wc');
  }

  async closeModal() {
    let form = this.shadowRoot.querySelector('form-persona');
    form.reset();
    this.shadowRoot.querySelector('#exampleModal').style.display = 'none';
    this.shadowRoot.querySelector('#exampleModal').classList.remove('show');
    this.shadowRoot.querySelector('#exampleModal').classList.remove('modal-open-wc');
    this.personaSelected = {};
    await this.shadowRoot.querySelector('form-persona').setPersona({});
    this.modeUpdate = false;
  }

  savePersona() {
    let form = this.shadowRoot.querySelector('form-persona');
    if(form.validate()) {
      let tmp = [...this.personas];
      let data = form.getPersona();
      if(this.modeUpdate) {
        tmp = tmp.map(item => {
          if(data.id === item.id) {
            return data;
          }
          return item;
        });
      } else {
        data.id = this.makeid();
        tmp.unshift(data);
      }
      this.personas = tmp;
      this.requestUpdate();
      this.closeModal();
    }
  }

  onSearchUsers(userName) {
    clearTimeout(this.delayPressSearch); // doesn't matter if it's 0
    this.delayPressSearch = setTimeout(() => {
      this.personas = [];
      this.shadowRoot.querySelector('.alert').classList.remove('hide');
      console.log('query', userName);
      this.shadowRoot.querySelector('element-dm').loadUsers(userName);
    }, 500);
  }

  render() {
    return html`

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css"
      integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">

    <top-loading></top-loading>

    <header>
       <header-menu
        titleHeader = "GitHub Users"
        .options = "${this.options}"
        @option-menu-selected = "${this.openModal}"
        @on-search-user = "${({detail}) => this.onSearchUsers(detail)}">
       </header-menu>
    </header>

    <main>
      <div class="container">

        <div class="alert alert-info text-center" role="alert">
          Consultando la informacion de las perosnas
        </div>

        <div class="row">
          ${this.personas.map((item) => html`
          <div class="col-12 col-sm-12 col-md-6 col-lg-4 col-xl-3">
            <ficha-persona
              @on-remove-person="${({ detail }) => this.removePerson(detail)}"
              @on-edit-person="${({ detail }) => this.editPerson(detail)}"
              .persona="${item}">
            </ficha-persona>
          </div>
          `)}
        </div>
      </div>
    </main>

    <!-- Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Agregar persona</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close" @click= "${this.closeModal}">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
              <form-persona></form-persona>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" @click= "${this.closeModal}" data-dismiss="modal">Cancelar</button>
            <button type="button" class="btn btn-primary" @click = "${this.savePersona}" >Guardar</button>
          </div>
        </div>
      </div>
    </div>

    <element-dm
          autoloadUsers
          @before-load-users = "${() => this.beforeLoadUsers()}"
          @load-users-complete = "${({detail}) => this.loadUsers(detail)}" ></element-dm>

         `;
  }
}
