import { html, fixture, expect } from '@open-wc/testing';

import '../src/PersonaApp.js';

describe('PersonaApp', () => {
  let element;
  beforeEach(async () => {
    element = await fixture(html`
      <persona-app></persona-app>
    `);
  });

  it('renders a h1', () => {
    const h1 = element.shadowRoot.querySelector('.navbar-brand');
    expect(h1).to.exist;
    expect(h1.textContent).to.equal('App Persona');
  });

  it('passes the a11y audit', async () => {
    await expect(element).shadowDom.to.be.accessible();
  });
});
